package to.cs160.berkeley.edu.fsmapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.gmail.yuyang226.flickrj.sample.android.FlickrHelper;
import com.gmail.yuyang226.flickrj.sample.android.FlickrjActivity;
import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.REST;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.PhotosInterface;
import com.googlecode.flickrjandroid.photos.SearchParameters;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.ToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.Card;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;


import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

public class FSMActivity extends Activity{

    private DeckOfCardsManager mDeckOfCardsManager;
    private RemoteDeckOfCards mRemoteDeckOfCards;
    private RemoteResourceStore mRemoteResourceStore;
    private RemoteToqNotification mRemoteToqNotification;
    private DrawView drawview;
    private ToqBroadcastReceiver toqReceiver;
    Bitmap[] face_pics = new Bitmap[6];
    String[] names = {"Jack Weinberg","Michael Rossman","Jackie Goldberg",
            "Joan Baez","Art Goldberg", "Mario Savio"};
    String[] draws = {"Draw %FSM%", "Draw %Free Speech%", "Draw %SLATE%", "Draw a Megaphone",
            "Draw %Now%", "Express your own view of Free Speech in an drawing"};
    CardImage[] mCardImages = new CardImage[6];

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fsm);
        //Toq App
        mDeckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());
//        DeckOfCardsEventListener docListener = new DeckOfCardsEventListenerImpl();
//        mDeckOfCardsManager.addDeckOfCardsEventListener(docListener);
        mDeckOfCardsManager.addDeckOfCardsEventListener(new DeckOfCardsEventListener() {
            @Override
            public void onCardOpen(String s) {
                Log.e("Listen to card:",s);
                Toast.makeText(getApplicationContext(), "Card opened", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), FSMActivity.class);
                startActivity(intent);
            }

            @Override
            public void onCardVisible(String s) {
                Log.e("Listen to card:",s);
                Intent intent = new Intent(getApplicationContext(), FSMActivity.class);
                startActivity(intent);
                if(s == "card0"){

                }else if(s == "card1"){

                }else if(s == "card2"){

                }else if(s == "card3"){

                }else if(s == "card4"){

                }else if(s == "card5"){

                }

            }

            @Override
            public void onCardInvisible(String s) {

            }

            @Override
            public void onCardClosed(String s) {

            }

            @Override
            public void onMenuOptionSelected(String s, String s2) {

            }

            @Override
            public void onMenuOptionSelected(String s, String s2, String s3) {

            }
        });
        face_pics[0] = BitmapFactory.decodeResource(getResources(), R.drawable.jack_weinberg_toq);
        face_pics[1] = BitmapFactory.decodeResource(getResources(), R.drawable.michael_rossman_toq);
        face_pics[2] = BitmapFactory.decodeResource(getResources(), R.drawable.jackie_goldberg_toq);
        face_pics[3] = BitmapFactory.decodeResource(getResources(), R.drawable.joan_baez_toq);
        face_pics[4] = BitmapFactory.decodeResource(getResources(), R.drawable.art_goldberg_toq);
        face_pics[5] = BitmapFactory.decodeResource(getResources(), R.drawable.mario_savio_toq);
        for(int i = 0; i < 6; i++){
            face_pics[i] = Bitmap.createScaledBitmap(face_pics[i],250, 288, true);
        }
//        toqReceiver = new ToqBroadcastReceiver();
//        toqReceiver.onReceive(getApplicationContext(), getIntent());
        init();
        install();
        //Drawing App
        final LinearLayout back = (LinearLayout)findViewById(R.id.background);
        back.setBackgroundColor(0xFF666666);
        drawview = (DrawView)findViewById(R.id.canvas);
        drawview.setBackgroundColor(0xFFFEFEFE);
        drawview.setStroke(1);
        final Button button_red = (Button)findViewById(R.id.red);
        final Button button_blue = (Button)findViewById(R.id.blue);
        final Button button_green = (Button)findViewById(R.id.green);
        final Button button_white = (Button)findViewById(R.id.white);
        button_white.setBackgroundResource(R.drawable.eraser);
        final Button button_black = (Button)findViewById(R.id.black);
        final Button submit = (Button)findViewById(R.id.submit);
        button_red.setBackgroundColor(0xFFFF0000);
        button_blue.setBackgroundColor(0xFF0000FF);
        button_green.setBackgroundColor(0xFF00FF00);
//        button_white.setBackgroundColor(0xFFFEFEFE);
        button_black.setBackgroundColor(0xFF000000);
        button_red.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int selected_paint = 0xFFFF0000;
                drawview.setStroke(1);
                drawview.setColor(selected_paint);
            }
        });
        button_blue.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int selected_paint = 0xFF0000FF;
                drawview.setStroke(1);
                drawview.setColor(selected_paint);
            }
        });
        button_green.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int selected_paint = 0xFF00FF00;
                drawview.setStroke(1);
                drawview.setColor(selected_paint);
            }
        });
        button_white.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int selected_paint = 0xFFFEFEFE;
                drawview.setStroke(0);
                drawview.setColor(selected_paint);
            }
        });
        button_black.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                int selected_paint = 0xFF000000;
                drawview.setStroke(1);
                drawview.setColor(selected_paint);
            }
        });
        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                File myDir = new File("");
                if(isExternalStorageWritable()){
                    Log.e("Writable Status", "True");
                    myDir = new File(getAlbumStorageDir("FSM"));
                }
                Log.e("Custom_log", "Before");
                myDir.mkdirs();
                Log.e("Custom_log", "After");
                int n = 10000;
                Random generator = new Random();
                String filename = "FSM_bitmap" + generator.nextInt(n) + ".jpg";
                File file = new File(myDir, filename);
                try {
                    FileOutputStream fos = new FileOutputStream(file);
                    Bitmap bm = Bitmap.createScaledBitmap(drawview.getBitmap(),250,288,true);
                    bm.compress(Bitmap.CompressFormat.PNG,100,fos);
                    fileUri = file;
                    fos.flush();
                    fos.close();
                    Log.e("Custom_log", "Image saved!");
                } catch (FileNotFoundException e) {
                    Log.e("FileNotFoundException:", "in sumbit");
//                    e.printStackTrace();
                } catch (IOException e) {
                    Log.e("IOException:", "in sumbit");
//                    e.printStackTrace();
                }
//
                Log.e("Custom_log:", "Before showImage()");
                showImage();
                Log.e("Custom_log:", "After showImage()");
                Log.e("Custom_log:", fileUri.getAbsolutePath());

                uploadPhoto();
//
                Log.e("Custom_log:", "After UploadPhoto()");

            }
        });

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
//                Toast.makeText(getApplicationContext(),
//                        "Your Location\n" + "Lat: " + latitude +"\nLong: " + longitude,
//                        Toast.LENGTH_SHORT ).show();
//                longitude ==  37.86965  && latitude == -122.25914
                if(latitude <=  37.87000 && latitude >= 37.86500 &&
                        longitude >= -122.26000 && longitude <= -122.25514){
                    Toast.makeText(getApplicationContext(),
                            "Notification!!!!!!!!",
                            Toast.LENGTH_SHORT ).show();
//                if(latitude > 0 && longitude < 0){
                    long millis = System.currentTimeMillis() % 1000;
                    String title = "FSM Project";
                    int r = (int)(millis%6);
                    String[] message = {"Drawing request:\n" + names[r]};
                    NotificationTextCard note = new NotificationTextCard(millis, title, message);
                    note.setVibeAlert(true);
                    mRemoteToqNotification = new RemoteToqNotification(getApplicationContext(), note);
                    try {
                        mDeckOfCardsManager.sendNotification(mRemoteToqNotification);
                    }catch(RemoteDeckOfCardsException e){
                        Log.e("RemoteDeckOfCardsException: ", "inside location listener");
                        e.printStackTrace();
                    }
                }
//                try {
//                    updateDeckOfCardsFromUI();
//                    mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
//                }catch(RemoteDeckOfCardsException e){
//                    e.printStackTrace();
//                }
//                Toast.makeText(getApplicationContext(),
//                        "Your Location\n" + "Lat: " + latitude +"\nLong: " + longitude,
//                        Toast.LENGTH_SHORT ).show();
//                output.setText("Your location is - \n Lat: " + latitude + "\nLong: " + longitude);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        // Register the listener with the Location Manager to receive location updates
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 180000, 0, locationListener);
        }

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 180000, 0, locationListener);
        }
    }

    private void init(){
        mRemoteResourceStore= new RemoteResourceStore();

        try {
            mRemoteDeckOfCards = createDeckOfCards();
        }
        catch(Throwable th){
            Log.e("Throwable: ", "in init()");
//            th.printStackTrace();
        }
    }

    private RemoteDeckOfCards createDeckOfCards(){
        ListCard listCard = new ListCard();
        for(int i= 0; i < 7; i ++){
            SimpleTextCard simpleTextCard = new SimpleTextCard("card" + i);
            simpleTextCard.setReceivingEvents(true);
            listCard.add(simpleTextCard);

            Log.e("card number", i + "");
        }
        return new RemoteDeckOfCards(this, listCard);
    }

    private void install(){
        updateDeckOfCardsFromUI();
//        try {
//            mDeckOfCardsManager.uninstallDeckOfCards();
//        }catch (RemoteDeckOfCardsException e ){
//            e.printStackTrace();
//            Log.e("RemoteDeckOfCardsException: ", "inside install when uninstalling");
//        }
        try {
//
            mDeckOfCardsManager.installDeckOfCards(mRemoteDeckOfCards,mRemoteResourceStore);
        }
        catch(RemoteDeckOfCardsException e){
//            try {
//                mDeckOfCardsManager.uninstallDeckOfCards();
//            }catch(RemoteDeckOfCardsException th){
//            e.printStackTrace();
//            Toast.makeText(this, "Application already installed", Toast.LENGTH_SHORT).show();
//            }
            e.printStackTrace();
            Log.e("RemoteDeckOfCardsException: ", "inside install when installing");
            Toast.makeText(this, "Application already installed", Toast.LENGTH_SHORT).show();

        }
    }

    private void updateDeckOfCardsFromUI(){
//        try {
//            mDeckOfCardsManager.uninstallDeckOfCards();
//        }catch (RemoteDeckOfCardsException e){
//            e.printStackTrace();
//        }
        if(mRemoteDeckOfCards == null){
            mRemoteDeckOfCards = createDeckOfCards();
        }
        //Card 1
        ListCard listCard = mRemoteDeckOfCards.getListCard();
//        SimpleTextCard simpleTextCard= (SimpleTextCard)listCard.childAtIndex(0);
//        simpleTextCard.setHeaderText("Hello World");
//        simpleTextCard.setTitleText("World world world");
//        String[] messages = {"Hello hello hello"};
//        simpleTextCard.setMessageText(messages);
//        simpleTextCard.setReceivingEvents(false);
//        simpleTextCard.setShowDivider(true);
        for(int i = 0; i < 6; i++) {
            Log.e("index", "" + i);
            SimpleTextCard simpleTextCard = (SimpleTextCard) listCard.childAtIndex(i);
            String name = names[i];
            simpleTextCard.setHeaderText(names[i]);
            simpleTextCard.setTitleText(names[i]);
            String[] messages = {draws[i].replaceAll("%","\"")};

            mCardImages[i] = new CardImage(names[i], face_pics[i]);
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[i]);
            simpleTextCard.setReceivingEvents(true);
            simpleTextCard.setMessageText(messages);
            simpleTextCard.setShowDivider(true);
        }
    }

    private void updateLastCardFromUI(Bitmap bm){
        ListCard listCard = mRemoteDeckOfCards.getListCard();
        SimpleTextCard simpleTextCard= (SimpleTextCard)listCard.childAtIndex(6);
        simpleTextCard.setHeaderText("FSM Project");
        simpleTextCard.setTitleText("Your reward card");
        String[] messages = {"Thank you for participating"};
        bm = Bitmap.createScaledBitmap(bm, 250,288,true);
        CardImage ret_photo = new CardImage("ret_photo", bm);
        simpleTextCard.setCardImage(mRemoteResourceStore,ret_photo);
        simpleTextCard.setMessageText(messages);
        simpleTextCard.setReceivingEvents(false);
        simpleTextCard.setShowDivider(true);
    }

    protected void onStart(){
        super.onStart();

        // If not connected, try to connect
        if (!mDeckOfCardsManager.isConnected()){
            try{
                mDeckOfCardsManager.connect();
//                init();
//                install();
            }
            catch (RemoteDeckOfCardsException e){
                Log.e("RemoteDeckOfCardsException: ", "inside onStart");
//                e.printStackTrace();
            }
        }
    }

    File fileUri;


    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private void showImage() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    String svr="www.flickr.com";

                    REST rest=new REST();
                    rest.setHost(svr);

                    //initialize Flickr object with key and rest
                    Flickr flickr=new Flickr(FlickrHelper.API_KEY,rest);

                    //initialize SearchParameter object, this object stores the search keyword
                    SearchParameters searchParams=new SearchParameters();
                    searchParams.setSort(SearchParameters.INTERESTINGNESS_DESC);

                    //Create tag keyword array
                    String[] tags=new String[]{"cs160fsm"};
                    searchParams.setTags(tags);

                    //Initialize PhotosInterface object
                    PhotosInterface photosInterface=flickr.getPhotosInterface();
                    //Execute search with entered tags
                    PhotoList photoList=photosInterface.search(searchParams,20,1);

                    //get search result and fetch the photo object and get small square imag's url
                    if(photoList!=null){
                        //Get search result and check the size of photo result
                        Random random = new Random();
                        int seed = random.nextInt(photoList.size());
                        //get photo object
                        Photo photo=(Photo)photoList.get(seed);

                        //Get small square url photo
                        InputStream is = photo.getMediumAsStream();
                        final Bitmap bm = BitmapFactory.decodeStream(is);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateLastCardFromUI(bm);
                                try {
                                    mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
                                }catch (RemoteDeckOfCardsException e){
                                    Log.e("RemoteDeckOfCardsException: ", "in showImage()");
                                    e.printStackTrace();
                                }
//                                ImageView imageView = (ImageView) findViewById(R.id.imview);
//                                imageView.setImageBitmap(bm);
                            }
                        });
                    }
                } catch (ParserConfigurationException e) {
                    Log.e("ParserConfigurationException: ", "in showImage()");
                    e.printStackTrace();
                } catch (FlickrException e) {
                    Log.e("FlickrException: ", "in showImage()");
                    e.printStackTrace();
                } catch (IOException e ) {
                    Log.e("IOException: ", "in showImage()");
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.e("JSONException: ", "in showImage()");
                    e.printStackTrace();
                }
            }
        };

        thread.start();
    }


    private void uploadPhoto(){
        Log.e("Custom_log:", fileUri.getAbsolutePath());
        if (fileUri == null) {
            Toast.makeText(getApplicationContext(), "Please pick photo",
                    Toast.LENGTH_SHORT).show();

            drawview = (DrawView) findViewById(R.id.canvas);
            Bitmap bitmap = drawview.getBitmap();
            Intent intent = new Intent(getApplicationContext(),
                    FlickrjActivity.class);
            intent.putExtra("flickImage", bitmap);
            startActivity(intent);
        } else {
            Log.e("Custom_log:", fileUri.getAbsolutePath());
            Intent intent = new Intent(getApplicationContext(),
                    FlickrjActivity.class);
            intent.putExtra("flickImagePath", fileUri.getAbsolutePath());
            Log.e("Upload photo path: ", fileUri.getAbsolutePath());
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.fsm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public boolean isExternalStorageWritable(){
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)){
            return true;
        }
        return false;
    }
    public boolean isExternalStorageReadable(){
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
    public String getAlbumStorageDir(String albumName){
        String path = Environment.getExternalStorageDirectory().toString() + "/" + albumName;
        Log.e("The file is",path);
        Log.e("The file is",albumName );
        return path;
    }

    // Handle card events triggered by the user interacting with a card in the installed deck of cards
    private class DeckOfCardsEventListenerImpl implements DeckOfCardsEventListener{

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardOpen(java.lang.String)
         */
        public void onCardOpen(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    Toast.makeText(FSMActivity.this, "Event Open: " + cardId, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardVisible(java.lang.String)
         */
        public void onCardVisible(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    Toast.makeText(FSMActivity.this, "Event Visible" + cardId, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardInvisible(java.lang.String)
         */
        public void onCardInvisible(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    Toast.makeText(FSMActivity.this, "Event Invisible" + cardId, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onCardClosed(java.lang.String)
         */
        public void onCardClosed(final String cardId){
            runOnUiThread(new Runnable(){
                public void run(){
                    Toast.makeText(FSMActivity.this, "Event Closed" + cardId, Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onMenuOptionSelected(java.lang.String, java.lang.String)
         */
        public void onMenuOptionSelected(final String cardId, final String menuOption){
            runOnUiThread(new Runnable(){
                public void run(){
                    Toast.makeText(FSMActivity.this, "Event Menu Options Selected" + cardId + " [" + menuOption + "]", Toast.LENGTH_SHORT).show();
                }
            });
        }

        /**
         * @see com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener#onMenuOptionSelected(java.lang.String, java.lang.String, java.lang.String)
         */
        public void onMenuOptionSelected(final String cardId, final String menuOption, final String quickReplyOption){
            runOnUiThread(new Runnable(){
                public void run(){
                    Toast.makeText(FSMActivity.this, "Event Menu Options Selected" + cardId + " [" + menuOption + ":" + quickReplyOption +
                            "]", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}

