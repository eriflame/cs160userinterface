package to.cs160.berkeley.edu.fsmapp;

/**
 * Created by Trung on 9/27/2014.
 */
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.util.Log;

public class DrawView extends View{
    // Paint
    private Paint drawPaint;
    // Holds draw calls
    private Canvas drawCanvas;
    // Bitmap to hold pixels
    private Bitmap canvasBitmap;
    float startX, startY;
    Path path = new Path();

    public DrawView(Context context, AttributeSet attrs){
        super(context, attrs);
        setupDrawing();

    }
    private void setupDrawing() {
        int paintColor = 0xFF000000;

        // Setup the drawing area for user interaction
        drawPaint = new Paint();
        // Change the color of paint to use
        drawPaint.setColor(paintColor);
        drawPaint.setStyle(Paint.Style.STROKE);
    }
    @Override
    protected void onDraw(Canvas canvas){
        canvas.drawBitmap(canvasBitmap, 0, 0, drawPaint);

    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // Make a new bitmap that stores each pixel on 4 bytes
        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();
        Path tempPath = new Path();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = touchX;
                startY = touchY;
                drawCanvas.drawPath(tempPath, drawPaint);
                break;
            case MotionEvent.ACTION_MOVE:
                tempPath.moveTo(startX,startY);
                tempPath.lineTo(touchX,touchY);
                drawCanvas.drawPath(tempPath, drawPaint);
                startX = touchX;
                startY = touchY;
                break;
            case MotionEvent.ACTION_UP:
                tempPath.moveTo(startX,startY);
                tempPath.lineTo(touchX,touchY);
                drawCanvas.drawPath(tempPath, drawPaint);
                path.close();
                break;
            default:
                return false;
        }
        invalidate();
        return true;
    }
    public void setColor(int paint_color){
        this.drawPaint.setColor(paint_color);
    }
    public void setStroke(int x){
        switch(x){
            case 0:
                this.drawPaint.setStrokeWidth(50);
//                this.drawPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                break;
            case 1:
//                drawPaint.setXfermode(null);
                this.drawPaint.setStrokeWidth(5);

        }
    }
    public Bitmap getBitmap(){
        return canvasBitmap;
    }
}
