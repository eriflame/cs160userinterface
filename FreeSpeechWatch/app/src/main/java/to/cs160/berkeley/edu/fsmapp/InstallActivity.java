package to.cs160.berkeley.edu.fsmapp;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.Card;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;


public class InstallActivity extends Activity {

    private DeckOfCardsManager mDeckOfCardsManager;
    private RemoteDeckOfCards mRemoteDeckOfCards;
    private RemoteResourceStore mRemoteResourceStore;
    private RemoteToqNotification mRemoteToqNotification;
    Bitmap[] face_pics = new Bitmap[6];
    String[] names = {"Jack Weinberg","Michael Rossman","Jackie Goldberg",
            "Joan Baez","Art Goldberg", "Mario Savio"};
    String[] draws = {"Draw %FSM%", "Draw %Free Speech%", "Draw %SLATE%", "Draw a Megaphone",
            "Draw %Now%", "Express your own view of Free Speech in an drawing"};
    CardImage[] mCardImages = new CardImage[6];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_install);
        face_pics[0] = BitmapFactory.decodeResource(getResources(), R.drawable.jack_weinberg_toq);
        face_pics[1] = BitmapFactory.decodeResource(getResources(), R.drawable.michael_rossman_toq);
        face_pics[2] = BitmapFactory.decodeResource(getResources(), R.drawable.jackie_goldberg_toq);
        face_pics[3] = BitmapFactory.decodeResource(getResources(), R.drawable.joan_baez_toq);
        face_pics[4] = BitmapFactory.decodeResource(getResources(), R.drawable.art_goldberg_toq);
        face_pics[5] = BitmapFactory.decodeResource(getResources(), R.drawable.mario_savio_toq);
        for(int i = 0; i < 6; i++){
            face_pics[i] = Bitmap.createScaledBitmap(face_pics[i],250, 288, true);
        }
        mDeckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());
        mDeckOfCardsManager.addDeckOfCardsEventListener(new DeckOfCardsEventListener() {
            @Override
            public void onCardOpen(String s) {
                Log.e("Listen to card:",s);
                Toast.makeText(getApplicationContext(), "Card opened", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), FSMActivity.class);
                startActivity(intent);
            }

            @Override
            public void onCardVisible(String s) {
                Log.e("Listen to card:",s);
                Intent intent = new Intent(getApplicationContext(), FSMActivity.class);
                startActivity(intent);
                if(s == "card0"){

                }else if(s == "card1"){

                }else if(s == "card2"){

                }else if(s == "card3"){

                }else if(s == "card4"){

                }else if(s == "card5"){

                }

            }

            @Override
            public void onCardInvisible(String s) {

            }

            @Override
            public void onCardClosed(String s) {

            }

            @Override
            public void onMenuOptionSelected(String s, String s2) {

            }

            @Override
            public void onMenuOptionSelected(String s, String s2, String s3) {

            }
        });
        init();

        findViewById(R.id.btn_hello).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                install();
            }
        });

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
//                Toast.makeText(getApplicationContext(),
//                        "Your Location\n" + "Lat: " + latitude +"\nLong: " + longitude,
//                        Toast.LENGTH_SHORT ).show();
//                longitude ==  37.86965  && latitude == -122.25914
                if(latitude <=  37.88000 && latitude >= 37.86000 &&
                            longitude >= -122.27000 && longitude <= -122.25414){
//                if(latitude > 0 && longitude < 0){
                    long millis = System.currentTimeMillis() % 1000;
                    String title = "FSM Project";
                    int r = (int)(millis%6);
                    String[] message = {"Drawing request:\n" + names[r]};
                    NotificationTextCard note = new NotificationTextCard(millis, title, message);
                    note.setVibeAlert(true);
                    mRemoteToqNotification = new RemoteToqNotification(getApplicationContext(), note);
                    try {
                        mDeckOfCardsManager.sendNotification(mRemoteToqNotification);
                    }catch(RemoteDeckOfCardsException e){
                        Log.e("RemoteDeckOfCardsException: ", "inside location listener");
                        e.printStackTrace();
                    }
                }
//                try {
//                    updateDeckOfCardsFromUI();
//                    mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
//                }catch(RemoteDeckOfCardsException e){
//                    e.printStackTrace();
//                }
//                Toast.makeText(getApplicationContext(),
//                        "Your Location\n" + "Lat: " + latitude +"\nLong: " + longitude,
//                        Toast.LENGTH_SHORT ).show();
//                output.setText("Your location is - \n Lat: " + latitude + "\nLong: " + longitude);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        // Register the listener with the Location Manager to receive location updates
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 0, locationListener);
        }

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 0, locationListener);
        }
    }

    private void install() {
        updateDeckOfCardsFromUI();
        try{
            mDeckOfCardsManager.installDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
        }
        catch (RemoteDeckOfCardsException e){
            e.printStackTrace();
            Toast.makeText(this, "Application already installed", Toast.LENGTH_SHORT).show();
        }
    }


    private void updateDeckOfCardsFromUI() {

        if(mRemoteDeckOfCards == null){
            mRemoteDeckOfCards = createDeckOfCards();
        }
        //Card 1
        ListCard listCard = mRemoteDeckOfCards.getListCard();
//        SimpleTextCard simpleTextCard= (SimpleTextCard)listCard.childAtIndex(0);
//        simpleTextCard.setHeaderText("Hello World");
//        simpleTextCard.setTitleText("World world world");
//        String[] messages = {"Hello hello hello"};
//        simpleTextCard.setMessageText(messages);
//        simpleTextCard.setReceivingEvents(false);
//        simpleTextCard.setShowDivider(true);
        for(int i = 0; i < 6; i++) {
            Log.e("index", "" + i);
            SimpleTextCard simpleTextCard = (SimpleTextCard) listCard.childAtIndex(i);
            String name = names[i];
            simpleTextCard.setHeaderText(names[i]);
            simpleTextCard.setTitleText(names[i]);
            String[] messages = {draws[i].replaceAll("%","\"")};

            mCardImages[i] = new CardImage(names[i], face_pics[i]);
            simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[i]);
            simpleTextCard.setReceivingEvents(true);
            simpleTextCard.setMessageText(messages);
            simpleTextCard.setShowDivider(true);
        }
    }
    //
    // Create some cards with example content
    private RemoteDeckOfCards createDeckOfCards(){

        ListCard listCard = new ListCard();
        for(int i= 0; i < 7; i ++){
            SimpleTextCard simpleTextCard = new SimpleTextCard("card" + i);
            simpleTextCard.setReceivingEvents(true);
            listCard.add(simpleTextCard);

            Log.e("card number", i + "");
        }
        return new RemoteDeckOfCards(this, listCard);
    }

    //    Initialise
    private void init(){

        // Create the resourse store for icons and images
        mRemoteResourceStore= new RemoteResourceStore();
        // Try to retrieve a stored deck of cards
        try {
            mRemoteDeckOfCards = createDeckOfCards();
        }
        catch (Throwable th){
            th.printStackTrace();
        }
    }

    /**
     * @see android.app.Activity#onStart()
     */
    protected void onStart(){
        super.onStart();

        // If not connected, try to connect
        if (!mDeckOfCardsManager.isConnected()){
            try{
                mDeckOfCardsManager.connect();
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.install, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
