# This imports all the layers for "top_menu" into catergoriesLayers
catergoriesLayers = Framer.Importer.load "imported/top_menu"


for layerGroupName of catergoriesLayers
	console.log(layerGroupName)
	window[layerGroupName] = catergoriesLayers[layerGroupName]
profile_groups = [curious, offline, next]
special_button = [close]
layerArr = [food, cafe, bar, museum, festival, romance]
smalllayerArr = [smallfood, smallcafe, smallbar, smallmuseum, smallfestival]
places = [rings, fosil, bluebottle, ritual, fourbarels, sightglass]
smallplaces = [smallsightglass, smallfourbarels, smallritual]
curious_profiles = [amy, dannis, guss, helen, john, lux, ras, sarah, william]
check_on_profile = [false, false, false, false, false, false, false, false, false]
offline_profiles = [alex, bo, cecil, flores, honey, sohan]
curve1 = "spring(300,20,50)"
splashCurve = 'spring(60,12,0)'
fade = (layer) ->
	layer.animate
		properties: {scale: 1, opacity: 0.0}
		time: 0.15
		curve: 'linear'
unfade = (layer) ->
	layer.animate
		properties: {scale: 1, opacity: 1}
		time: 0.15
		curve: 'linear'
selected = (layer) ->
	layer.animate
		properties: {scale: 1.1, opacity: 0}
		time: 0.15
		curve: 'linear'
notselected = (layer) ->
	layer.animate
		properties: {scale: 0.9, opacity: 0}
		time: 0.15
		curve: 'linear'
check = (layer) ->
	layer.animate
		properties: {scale: 1.2}
		time: 0.15
		curve: 'linear'
uncheck = (layer) ->
	layer.animate
		properties: {scale: 1}
		time: 0.15
		curve: 'linear'
pull_up = (layer, y_value) ->
	layer.animate
		properties: {y: y_value, opacity: 1}
		time: 0.15
		curve: curve1
popin = (layer) ->
	layer.animate
		properties: {opacity: 1}
		time: 0.15
		curve: "pop"
shiftleft = (layer, xvalue, yvalue) ->
	animation = layer.animate
		properties: {x: xvalue, y: yvalue}
		time: 0.0
		curve: splashCurve
	return animation
scaling = (layer, s) ->
	layer.animate
		properties: {scale: s}
		time: 0.01
		curve: splashCurve		
`var unfadeAll = function(array){
	for(var i = 0, len = array.length; i < len; i++){
			unfade(array[i])
	}
}`
`var placeLayersBefore = function(fronts, backs){
	for(var i = 0; i < backs.length; i++){
		for(var j = 0; j < fronts.length; j++){
			backs[i].placeBefore(fronts[j])
		}
	}
}`
`var destroyLayers = function(array){
	for(var i = 0; i < array.length; i++){
		array[i].destroy()
	}
}`
`var getPlaceMenu = function(index){
	for(var i = 0; i < places.length; i++){
		pull_up(places[i], 300 + i*150)
	}
	unfade(smalllayerArr[index])
	placeLayersBefore(layerArr, places)
}`
`var getProfileMenu = function(index){
	pull_up(curious, 200)
	pull_up(next, 230)
	for(var i = 0; i < curious_profiles.length; i+=3){
		
		pull_up(curious_profiles[i], 330 + 80*i)
		pull_up(curious_profiles[i+1], 330 + 80*i)
		pull_up(curious_profiles[i+2], 330 + 80*i)

	}
	pull_up(offline, 1100)
	for(var i = 0; i < offline_profiles.length; i+=3){
		pull_up(offline_profiles[i], 1230 + 80*i)
		pull_up(offline_profiles[i+1], 1230 + 80*i)
		pull_up(offline_profiles[i+2], 1230 + 80*i)

	}
	unfadeAll(curious_profiles)
	unfadeAll(offline_profiles)
	unfade(curious)
	unfade(offline)
	unfade(smallplaces[index])
	placeLayersBefore(places, curious_profiles)
	placeLayersBefore(places, profile_groups)
}`

`var unfade_transition = function(index, array, menu){
	selected(array[index])
	for(var i = 0, len = array.length; i < len; i++){
		if(index != i){
			notselected(array[i]);
		}
	}
	destroyLayers(array)
	if(menu == 1){
		getPlaceMenu(index)
	}else if(menu == 2){
		getProfileMenu(index)
	}
	
}`
`var fade_transition = function(index, array){
	fade(array[index])
}`
`var fadeAll = function(array){
	for(var i = 0, len = array.length; i < len; i++){
			fade(array[i])
	}
}`
`var check_transition = function(index){
	if(check_on_profile[index]){
		uncheck(curious_profiles[index])
		check_on_profile[index] = false
	}else{
		check(curious_profiles[index])
		check_on_profile[index] = true
	}
}`
`var reset = function(){
	fadeAll(smalllayerArr)
	fadeAll(places)
	fadeAll(smallplaces)
	fadeAll(curious_profiles)
	fadeAll(offline_profiles)
	fade(curious)
	fade(offline)
	fade(next)
	fade(sendpopup)
	unfadeAll(layerArr)
	unfadeAll(special_button)
}`
reset()
unfade(background)
close.on Events.Click, ->
	reset()
food.on Events.Click, ->
	console.log("food")
	unfade_transition(0, layerArr,1)
cafe.on Events.Click, ->
	console.log("cafe")
	unfade_transition(1, layerArr,1)
bar.on Events.Click, ->
	console.log("bar")
	unfade_transition(2, layerArr,1)
#Missing Musium, Festival, Romance

rings.on Events.Click, ->
	console.log("rings")
	unfade_transition(0, places, 2)
fosil.on Events.Click, ->
	console.log("fosil")
	unfade_transition(1, places, 2)
bluebottle.on Events.Click, ->
	console.log("bluebottle")
	unfade_transition(2, places, 2)
ritual.on Events.Click, ->
	console.log("ritual")
	unfade_transition(3, places, 2)
#Missing fourbarels, sightglass
amy.on Events.Click, ->
	console.log("Amy")
	check_transition(0)
dannis.on Events.Click, ->
	console.log("Dannis")
	check_transition(1)
guss.on Events.Click, ->
	console.log("Guss")
	check_transition(2)
helen.on Events.Click, ->
	console.log("Helen")
	check_transition(3)
john.on Events.Click, ->
	console.log("John")
	check_transition(4)
lux.on Events.Click, ->
	console.log("Lux")
	check_transition(5)
ras.on Events.Click, ->
	console.log("Ras")
	check_transition(6)
sarah.on Events.Click, ->
	console.log("Sarah")
	check_transition(7)
william.on Events.Click, ->
	console.log("William")
	check_transition(8)
next.on Events.Click, ->
	shiftleft(curious, -1500, 0)
	shiftleft(next,  -1500, 0)
	shiftleft(offline, -1500, 0)
	shiftleft(amy, -1500, 0)
	shiftleft(dannis, -1500, 0)
	shiftleft(guss, -1500, 0)
	shiftleft(helen, -1500, 0)
	shiftleft(john, -1500, 0)
	shiftleft(lux, -1500, 0)
	shiftleft(ras, -1500, 0)
	shiftleft(sarah, -1500, 0)
	shiftleft(william, -1500, 0)
	shiftleft(alex, -1500, 0)
	shiftleft(bo, -1500, 0)
	shiftleft(cecil, -1500, 0)
	shiftleft(flores, -1500, 0)
	shiftleft(honey, -1500, 0)
	shiftleft(sohan, -1500, 0)
	shiftleft(yen, -1500, 0)
	unfade(timewheel)
	unfade(start)
	unfade(cancel)
	shiftleft(timewheel, 15, 300)
	shiftleft(start, 450, 900)
	shiftleft(cancel, 100, 900)
start.on Events.MouseOver, ->
	scaling(start,1.3)
start.on Events.MouseOut, ->
	scaling(start,1)
cancel.on Events.MouseOver, ->
	scaling(cancel,1.3)
cancel.on Events.MouseOut, ->
	scaling(cancel,1)
start.on Events.Click, ->
	shiftleft(timewheel, -1500, 0)
	shiftleft(start, -1500, 0)
	shiftleft(cancel, -1500, 0)
	unfade(features)
	shiftleft(features, 15, 300)
feature_check = [false, false, false, false, false, false, false, false]
feature_layers = [airplane, bike, funny, mechanic, golf, factory, food, hospital]
`var reset = function(){
	fadeAll(smalllayerArr)
	fadeAll(places)
	fadeAll(smallplaces)
	fadeAll(curious_profiles)
	fadeAll(offline_profiles)
	fade(curious)
	fade(offline)
	fade(next)
	fade(sendpopup)
	unfadeAll(layerArr)
	unfadeAll(special_button)
}`

`var resetscale = function(index, array){
	for(var i = 0, len = array.length; i < len - 1; i++){
		if(i != index){
			scaling(array[i],1)
		}
	}
}` 
initx = slidebob.x
inity = slidebob.y
airplane.on Events.Click, ->
	scaling(airplane, 1.3)
	resetscale(0, feature_layers)
	shiftleft(slidebob, initx, inity)
bike.on Events.Click, ->
	scaling(bike, 1.3)
	resetscale(1, feature_layers)
	shiftleft(slidebob, initx, inity)
funny.on Events.Click, ->
	scaling(funny, 1.3)
	resetscale(2, feature_layers)
	shiftleft(slidebob, initx, inity)
mechanic.on Events.Click, ->
	scaling(mechanic, 1.3)
	resetscale(3, feature_layers)
	shiftleft(slidebob, initx, inity)
golf.on Events.Click, ->
	scaling(golf, 1.3)
	resetscale(4, feature_layers)
	shiftleft(slidebob, initx, inity)
factory.on Events.Click, ->
	scaling(factory, 1.3)
	resetscale(5, feature_layers)
	shiftleft(slidebob, initx, inity)
food.on Events.Click, ->
	scaling(food, 1.3)
	resetscale(6, feature_layers)
	shiftleft(slidebob, initx, inity)
hospital.on Events.Click, ->
	scaling(hospital, 1.3)
	resetscale(7, feature_layers)
	shiftleft(slidebob, initx, inity)
slidebob.draggable.enabled = true
confirm.on Events.Click, ->
	fade(confirm)
	shiftleft(timewheel, -1500, 0)
	shiftleft(start, -1500, 0)
	shiftleft(cancel, -1500, 0)
	shiftleft(features, 0, -5000)
	unfade(sendpopup)
	shiftleft(sendpopup, 20, 530)


	

	 
	
	