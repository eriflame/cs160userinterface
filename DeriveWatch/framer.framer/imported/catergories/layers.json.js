window.__imported__ = window.__imported__ || {};
window.__imported__["catergories/layers.json.js"] = [
	{
		"id": 10,
		"name": "Group 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 750,
			"height": 1334
		},
		"maskFrame": null,
		"image": null,
		"imageType": null,
		"children": [
			{
				"id": 45,
				"name": "romance",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1334
				},
				"maskFrame": null,
				"image": {
					"path": "images/romance.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 750,
						"height": 1334
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1603059513"
			},
			{
				"id": 43,
				"name": "festival",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1334
				},
				"maskFrame": null,
				"image": {
					"path": "images/festival.png",
					"frame": {
						"x": 100,
						"y": 1140,
						"width": 551,
						"height": 156
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1017356833"
			},
			{
				"id": 41,
				"name": "museum",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1334
				},
				"maskFrame": null,
				"image": {
					"path": "images/museum.png",
					"frame": {
						"x": 100,
						"y": 916,
						"width": 551,
						"height": 156
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "840120744"
			},
			{
				"id": 39,
				"name": "park",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1334
				},
				"maskFrame": null,
				"image": {
					"path": "images/park.png",
					"frame": {
						"x": 100,
						"y": 916,
						"width": 551,
						"height": 156
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "745965133"
			},
			{
				"id": 37,
				"name": "bar",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1334
				},
				"maskFrame": null,
				"image": {
					"path": "images/bar.png",
					"frame": {
						"x": 99,
						"y": 692,
						"width": 551,
						"height": 156
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "709371867"
			},
			{
				"id": 35,
				"name": "cafe",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1334
				},
				"maskFrame": null,
				"image": {
					"path": "images/cafe.png",
					"frame": {
						"x": 99,
						"y": 468,
						"width": 551,
						"height": 156
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "728933918"
			},
			{
				"id": 33,
				"name": "food",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1334
				},
				"maskFrame": null,
				"image": {
					"path": "images/food.png",
					"frame": {
						"x": 99,
						"y": 226,
						"width": 551,
						"height": 156
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1688179520"
			},
			{
				"id": 29,
				"name": "close",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 750,
					"height": 1334
				},
				"maskFrame": null,
				"image": {
					"path": "images/close.png",
					"frame": {
						"x": 12,
						"y": 40,
						"width": 200,
						"height": 100
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2123703203"
			}
		],
		"modification": "1235771856"
	}
]