window.__imported__ = window.__imported__ || {};
window.__imported__["derivewatch/layers.json.js"] = [
	{
		"id": 6,
		"name": "background",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 999,
			"height": 800
		},
		"maskFrame": null,
		"image": {
			"path": "images/background.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 651,
				"height": 395
			}
		},
		"imageType": "png",
		"children": [
			{
				"id": 20,
				"name": "accept",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/accept.png",
					"frame": {
						"x": 182,
						"y": 249,
						"width": 98,
						"height": 98
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "795256579"
			},
			{
				"id": 18,
				"name": "decline",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/decline.png",
					"frame": {
						"x": 48,
						"y": 249,
						"width": 98,
						"height": 98
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "154064054"
			},
			{
				"id": 16,
				"name": "message",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/message.png",
					"frame": {
						"x": 26,
						"y": 62,
						"width": 268,
						"height": 146
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "2015643641"
			},
			{
				"id": 156,
				"name": "end",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/end.png",
					"frame": {
						"x": 68,
						"y": 474,
						"width": 131,
						"height": 27
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "154642501"
			},
			{
				"id": 88,
				"name": "finalscene",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": null,
				"imageType": null,
				"children": [
					{
						"id": 151,
						"name": "ark",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 999,
							"height": 800
						},
						"maskFrame": null,
						"image": {
							"path": "images/ark.png",
							"frame": {
								"x": 351,
								"y": 175,
								"width": 300,
								"height": 149
							}
						},
						"imageType": "png",
						"children": [
							{
								"id": 147,
								"name": "mslionboat",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 999,
									"height": 800
								},
								"maskFrame": null,
								"image": {
									"path": "images/mslionboat.png",
									"frame": {
										"x": 463,
										"y": 202,
										"width": 29,
										"height": 32
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "723591178"
							},
							{
								"id": 145,
								"name": "mrlionboat",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 999,
									"height": 800
								},
								"maskFrame": null,
								"image": {
									"path": "images/mrlionboat.png",
									"frame": {
										"x": 433,
										"y": 202,
										"width": 30,
										"height": 33
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "722876256"
							},
							{
								"id": 143,
								"name": "penguinfboat",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 999,
									"height": 800
								},
								"maskFrame": null,
								"image": {
									"path": "images/penguinfboat.png",
									"frame": {
										"x": 492,
										"y": 198,
										"width": 28,
										"height": 31
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "722816647"
							},
							{
								"id": 141,
								"name": "penguinmboat",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 999,
									"height": 800
								},
								"maskFrame": null,
								"image": {
									"path": "images/penguinmboat.png",
									"frame": {
										"x": 520,
										"y": 197,
										"width": 28,
										"height": 31
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "722727329"
							},
							{
								"id": 139,
								"name": "cockboat",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 999,
									"height": 800
								},
								"maskFrame": null,
								"image": {
									"path": "images/cockboat.png",
									"frame": {
										"x": 549,
										"y": 189,
										"width": 26,
										"height": 34
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "722608324"
							},
							{
								"id": 137,
								"name": "henboat",
								"layerFrame": {
									"x": 0,
									"y": 0,
									"width": 999,
									"height": 800
								},
								"maskFrame": null,
								"image": {
									"path": "images/henboat.png",
									"frame": {
										"x": 575,
										"y": 185,
										"width": 25,
										"height": 34
									}
								},
								"imageType": "png",
								"children": [
									
								],
								"modification": "721922886"
							}
						],
						"modification": "917942914"
					},
					{
						"id": 153,
						"name": "water",
						"layerFrame": {
							"x": 0,
							"y": 0,
							"width": 999,
							"height": 800
						},
						"maskFrame": null,
						"image": {
							"path": "images/water.png",
							"frame": {
								"x": 323,
								"y": 0,
								"width": 339,
								"height": 394
							}
						},
						"imageType": "png",
						"children": [
							
						],
						"modification": "720761153"
					}
				],
				"modification": "227825316"
			},
			{
				"id": 126,
				"name": "boatonmap",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/boatonmap.png",
					"frame": {
						"x": 592,
						"y": 310,
						"width": 47,
						"height": 23
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "696838949"
			},
			{
				"id": 120,
				"name": "pathtoboat",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/pathtoboat.png",
					"frame": {
						"x": 636,
						"y": 260,
						"width": 115,
						"height": 100
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "695915337"
			},
			{
				"id": 117,
				"name": "penguinonmap",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/penguinonmap.png",
					"frame": {
						"x": 627,
						"y": 238,
						"width": 20,
						"height": 23
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "695140772"
			},
			{
				"id": 113,
				"name": "pathtopenguin",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/pathtopenguin.png",
					"frame": {
						"x": 481,
						"y": 148,
						"width": 156,
						"height": 101
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "695021706"
			},
			{
				"id": 104,
				"name": "mrliononmap",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/mrliononmap.png",
					"frame": {
						"x": 565,
						"y": 130,
						"width": 24,
						"height": 28
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "693174788"
			},
			{
				"id": 107,
				"name": "pathtolion",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/pathtolion.png",
					"frame": {
						"x": 559,
						"y": 158,
						"width": 33,
						"height": 145
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1962020235"
			},
			{
				"id": 111,
				"name": "start",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/start.png",
					"frame": {
						"x": 574,
						"y": 299,
						"width": 30,
						"height": 34
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "692281020"
			},
			{
				"id": 102,
				"name": "ucbmap",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 999,
					"height": 800
				},
				"maskFrame": null,
				"image": {
					"path": "images/ucbmap.png",
					"frame": {
						"x": 318,
						"y": 0,
						"width": 681,
						"height": 554
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1342205242"
			}
		],
		"modification": "755781608"
	}
]