# This imports all the layers for "derivewatch" into derivewatchLayers
derivewatchLayers = Framer.Importer.load "imported/derivewatch"

for layerGroupName of derivewatchLayers
	console.log(layerGroupName)
	window[layerGroupName] = derivewatchLayers[layerGroupName]
curve1 = "spring(300,20,50)"
messageLayer = [ message, accept, decline ]
splashCurve = 'spring(60,12,0)'
fade = (layer) ->
	animation = layer.animate
		properties: {opacity: 0.0}
		time: 0.15
		curve: splashCurve
	return animation
unfade = (layer) ->
	animation = layer.animate
		properties: {opacity: 1}
		time: 0.15
		curve: splashCurve
	return animation

shiftleft = (layer, xvalue, yvalue) ->
	animation = layer.animate
		properties: {x: xvalue, y: yvalue}
		time: 0.0
		curve: splashCurve
	return animation
float = (layer, xvalue, yvalue) ->
	animation = layer.animate
		properties: {x: xvalue, y: yvalue}
		time: 10
		curve: 'linear'
	return animation

scaling = (layer, s) ->
	layer.animate
		properties: {scale: s}
		time: 0.15
		curve: splashCurve
direction = (layer, xvalue, yvalue) ->
	layer.animate
		properties: {x: xvalue, y: yvalue}
		time: 0.1
		curve: 'linear'
zoommap = (layer, s, xvalue, yvalue) ->
	animation = layer.animate
		properties: {scale: s, x: xvalue, y: yvalue}
		time: 0.0
		curve: splashCurve
	return animation
fade(ucbmap)
accept.on Events.MouseOver, ->
	console.log("Accept")
	scaling(accept,1.3)
accept.on Events.MouseOut, ->
	console.log("Accept")
	scaling(accept,1)
accept.on Events.Click, -> 
	console.log("Accept")
	fade(accept)
	fade(message)
	fade(decline)
	unfade(ucbmap)
	fade(start)
	fade(pathtolion)
	fade(mrliononmap)
	shiftleft(start, 190, 290)
	shiftleft(pathtolion, 180, 150)
	shiftleft(mrliononmap, 195, 140)
	shiftleft(accept, -100, 100)
	mapanimation = shiftleft(ucbmap,-200, 0)
	mapanimation.on "end", ->
		startanimation = unfade(start)
		startanimation.on "end", ->
			pathanimation = unfade(pathtolion)
			pathanimation.on "end", ->
				unfade(mrliononmap)	
	
mrliononmap.on Events.MouseOver, ->
	console.log("MrLion")
	scaling(mrliononmap,2)
mrliononmap.on Events.MouseOut, ->
	console.log("MrLion")
	scaling(mrliononmap,1)
	
mrliononmap.on Events.Click, ->
	console.log("MrLion")
	shiftleft(ucbmap, -300, 0)
	shiftleft(start, 90, 290)
	shiftleft(pathtolion,80,150)
	fade(pathtopenguin)
	fade(penguinonmap)
	shiftleft(pathtopenguin, 113, 170)
	shiftleft(penguinonmap, 260, 270)
	lionanimation = shiftleft(mrliononmap, 95, 140)
	lionanimation.on "end", ->
		pathanimation = unfade(pathtopenguin)
		pathanimation.on "end", ->
			unfade(penguinonmap)

penguinonmap.on Events.MouseOver, ->
	console.log("MrLion")
	scaling(penguinonmap,2)
penguinonmap.on Events.MouseOut, ->
	console.log("MrLion")
	scaling(penguinonmap,1)

penguinonmap.on Events.Click, ->
	console.log("Penguin")
	fade(pathtoboat)
	fade(boatonmap)
	shiftleft(ucbmap, -300, 0)
	shiftleft(start, -90, -290)
	shiftleft(pathtolion,-80,-150)
	shiftleft(mrliononmap, -90, -150)
	penanimation = shiftleft(pathtopenguin, -113, -170)
	zoommap(ucbmap,2.5, -880, -300)
	zoommap(pathtoboat,2.0, 115, 160)
	zoommap(boatonmap,1.0, 245, 160)
	zoommap(penguinonmap,2.5, 30, 100)
	penanimation.on "end", ->
		console.log("hello")
		pathanimation = unfade(pathtoboat)
		pathanimation.on "end", ->
			unfade(boatonmap)

boatonmap.on Events.MouseOver, ->
	console.log("MrLion")
	scaling(boatonmap,2)
boatonmap.on Events.MouseOut, ->
	console.log("MrLion")
	scaling(boatonmap,1)
	
boatonmap.on Events.Click, ->
	console.log("Boat")
	fade(ucbmap)
	fade(penguinonmap)
	fade(pathtoboat)
	fade(boatonmap)
	shiftleft(water,-330,0)
	fanimation = float(ark, -700, 200)
	fanimation.on "end", ->
		shiftleft(end, 100, 180)
	
penguinonmap.on Events.MouseOver, ->
	console.log("MrLion")
	scaling(penguinonmap,2)
penguinonmap.on Events.MouseOut, ->
	console.log("MrLion")
	scaling(penguinonmap,1)

	
	
	